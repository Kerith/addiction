﻿using UnityEngine;
using System.Collections;
using System;
using KWorks;

public class AimPushMovement : MonoBehaviour {

    private Transform tr;
    private Rigidbody2D rb;

    public float pushForce;
	[SerializeField]
	private float pushForceScalingFactor;
    
    void Start () {
        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();

		Messenger<float>.AddListener(this.name+" healthChanged", updatePushForceByHealth);
		Messenger<float, float>.AddListener (this.name + " boostForce", boostPushForceForAWhile);

    }    

    void FixedUpdate () 
	{
        Vector3 aimDirection = calculateAimDirection();
        rotateToFace(aimDirection);
        if (Input.GetButtonDown("Push"))
        { 			
			Vector2 pushVector = pushForce * new Vector2(aimDirection.x, aimDirection.y);
			rb.AddForce(pushVector);
            Messenger.Broadcast(gameObject.name + " pushing");    
        }       
	}

    private void rotateToFace(Vector3 target)
    {
        float angle = Mathf.Atan2(target.y, target.x) * Mathf.Rad2Deg;
        tr.rotation = Quaternion.LookRotation(Vector3.forward, target);
    }

    private Vector3 calculateAimDirection() 
	{
        if (Input.GetJoystickNames().Length > 0)        
            return InputUtils.pointingDirectionOfJoysticks();        

		return InputUtils.directionTowardsMouse (tr.position);
	}
  
	private void boostPushForceForAWhile(float rawPushForce, float boostDurationSeconds) {
		StartCoroutine (updatePushForceTemporarily (rawPushForce, boostDurationSeconds));
	}	

	private IEnumerator updatePushForceTemporarily(float rawPushForceDelta, float boostDurationSeconds)
	{
		updatePushForce (rawPushForceDelta);
		yield return new WaitForSeconds (boostDurationSeconds);
		updatePushForce (-rawPushForceDelta);
    }

    private void updatePushForceByHealth(float healthDelta)
    {
        updatePushForce(-healthDelta);
    }

    private void updatePushForce(float rawPushForceDelta)
    {
        pushForce += rawPushForceDelta * pushForceScalingFactor;
    }
}
