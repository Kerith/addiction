﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	[SerializeField]
	private float health;

    void Awake()
    {
        Messenger<float>.AddListener(this.name + " dealDamage", onDamage);
        Messenger<float>.AddListener(this.name + " heal", onHeal);
        Messenger.AddListener<float>(this.name + " requestHealth", getHealth);
    }


	public void onDamage(float damageAmount)
    {                
        health -= damageAmount;
		if (health <= 0.0f) {
            destroy();
			return;
		}
        Messenger<float>.Broadcast(this.name + " healthChanged", -damageAmount);
    }

	public void onHeal(float healAmount)
    {
		health += healAmount;		
		Messenger<float>.Broadcast (this.name+" healthChanged", healAmount);
	}

	private void destroy()
    {
        Death.die(this.gameObject);
	}

    public float getHealth()
    {
        return health;
    }
		
} 
