﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BurstParticlesOnMessages : MonoBehaviour {

    public string targetTag;
    [SerializeField]
    private List<string> messages;
    [SerializeField]
    private int numberOfParticles;

    private Transform target;

    private ParticleSystem ps;    

    void Start() {
        locateComponents();
        locateTarget();
        initializeListeners();
    }

    private void locateComponents()
    {
        ps = GetComponent<ParticleSystem>();
    }

    private void locateTarget()
    {
        target = GameObject.FindGameObjectWithTag(targetTag).GetComponent<Transform>();
        if (!target)
            Debug.Log("Invalid Target");
    }

    private void initializeListeners()
    {
        foreach (string message in messages)
            Messenger.AddListener(target.name + " " + message, turnOn);
    }

    private void turnOn()
    {
        ps.Emit(numberOfParticles);
    }
   
}
