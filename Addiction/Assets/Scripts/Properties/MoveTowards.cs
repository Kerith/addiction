﻿using UnityEngine;
using System.Collections;
using KWorks.Wrappers;

public class MoveTowards : MonoBehaviour {

    public string targetTag;
    private Transform targetTransform;

    private float speed;
    [SerializeField]
    private float speedMultiplyingCoefficient;

    private Transform tr;
    private Rigidbody2D rb;

    void Start () {        
        GameObject target = GameObject.FindGameObjectWithTag(targetTag);
        targetTransform = target.GetComponent<Transform>();
        Messenger.Broadcast<float>(target.name+" requestHealth", initializeSpeed);
        Messenger<float>.AddListener(targetTransform.name + " healthChanged", updateSpeed);

        tr = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Vector3 movementDirection = 
            Vector3.Normalize(targetTransform.position - tr.position);

        rb.SetVelocityX(movementDirection.x * speed);
        rb.SetVelocityY(movementDirection.y * speed);
    }

    private void initializeSpeed(float health)
    {
        speed = (health * speedMultiplyingCoefficient);
    }

    private void updateSpeed(float healthDelta)
    {
        speed -= healthDelta * speedMultiplyingCoefficient;        
    }


}
