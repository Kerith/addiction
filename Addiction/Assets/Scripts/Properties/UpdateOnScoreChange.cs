﻿using UnityEngine;
using System.Collections;
using System;

public class UpdateOnScoreChange : MonoBehaviour {

    public string targetTag;

    private GameObject target;

    private UnityEngine.UI.Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<UnityEngine.UI.Text>();

        target = GameObject.FindGameObjectWithTag(targetTag);
        Messenger<float>.AddListener(target.name + " scoreUpdate", updateText);
	}

    private void updateText(float newScore)
    {
        text.text = "Score: " + newScore;
    }

}
