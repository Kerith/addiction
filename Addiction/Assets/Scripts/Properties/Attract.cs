﻿using UnityEngine;
using System.Collections;

public class Attract : MonoBehaviour {

    public string targetTag;
    [SerializeField]
    private Transform entityToAttract;
    
    private float pullForce;
    [SerializeField]
    private float pullForceMultiplyingCoefficient;

    private Transform tr;

	void Start ()
    {        
        GameObject entity = GameObject.FindGameObjectWithTag(targetTag);
        entityToAttract = entity.GetComponent<Transform>();
        Messenger.Broadcast<float>(entityToAttract.name + " requestHealth", initializePullForce);
        Messenger<float>.AddListener(entityToAttract.name + " healthChanged", updatePullForce);

        tr = GetComponent<Transform>();
	}
    
    void Update()
    {       
        Vector2 attractionForce = calculateAttractionForce();

        Rigidbody2D otherEntitysRigidBody = 
            entityToAttract.GetComponent<Rigidbody2D>();

        otherEntitysRigidBody.AddForce(attractionForce);
    }	

    private void initializePullForce(float health)
    {
       pullForce = (health * pullForceMultiplyingCoefficient);
    }

    private void updatePullForce(float healthDelta)
    {
        pullForce -= 
            healthDelta * 
            pullForceMultiplyingCoefficient;
    }

    private Vector2 calculateAttractionForce()
    {
        Vector2 force = Vector2.zero;

        float distanceToTarget = Vector3.Distance(tr.position, entityToAttract.position);

        Vector3 attractionDirection =
            Vector3.Normalize(tr.position - entityToAttract.position);

        force.x = attractionDirection.x;
        force.y = attractionDirection.y;

        force *= pullForce * pullForceMultiplyingCoefficient;

        if (distanceToTarget >= 1)
            force /= Mathf.Pow(distanceToTarget, 2);

        return force;
    }
	
}
