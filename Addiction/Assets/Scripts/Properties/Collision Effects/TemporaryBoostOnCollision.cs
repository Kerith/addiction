﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TemporaryBoostOnCollision : MonoBehaviour {

    public List<string> tagsToCollideWith;
    public float boostAmount;
    public float boostTime;
    public string propertyToBoost;

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            Messenger<float, float>.Broadcast(
                col.gameObject.name+" boost"+propertyToBoost, boostAmount, boostTime);
        }
    }
}
