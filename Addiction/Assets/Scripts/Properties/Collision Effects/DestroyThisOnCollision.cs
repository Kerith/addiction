﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyThisOnCollision : MonoBehaviour {

    public List<string> tagsToCollideWith;

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            Death.die(this.gameObject);
        }
    }

}
