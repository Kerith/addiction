﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum HealthActionType
{
    HEAL,
    DAMAGE
}

public class HealthActionOnCollision : MonoBehaviour {

    public List<string> tagsToCollideWith;

    public float healthActionAmount;
    public bool repeatActionOverTime;
    public float repeatingIntervalSeconds;
    public HealthActionType healthActionType;    

    private IEnumerator healthCoroutine;
    private Transform playerCollidedWith;

    void Start()
    {
        setupHealthCoroutine();
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
           playerCollidedWith = col.transform; 
           StartCoroutine(healthCoroutine);                                           
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            playerCollidedWith = col.transform;
            StopCoroutine(healthCoroutine);
        }       
    }   

    private void setupHealthCoroutine()
    {
        switch(healthActionType)
        {
            case HealthActionType.DAMAGE:
                healthCoroutine = dealDamageRepeating();
                break;
            case HealthActionType.HEAL:
                healthCoroutine = healRepeating();
                break;
        }
    }

    private IEnumerator dealDamageRepeating()
    {
        dealDamage();
        while (repeatActionOverTime)
        {
            yield return new WaitForSeconds(repeatingIntervalSeconds);
            dealDamage();
        }

    }
    private IEnumerator healRepeating()
    {
        heal();
        while (repeatActionOverTime)
        {
            yield return new WaitForSeconds(repeatingIntervalSeconds);
            heal();
        }

    }

    private void dealDamage()
    {
        Messenger<float>.Broadcast(
                playerCollidedWith.name + " dealDamage", healthActionAmount);
    }

    private void heal()
    {
        Messenger<float>.Broadcast(
                playerCollidedWith.name + " heal", healthActionAmount);
    }
}
