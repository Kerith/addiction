﻿using UnityEngine;
using System.Collections;
using KWorks.Wrappers;
using System.Collections.Generic;

public class ChangeSizeOnCollision : MonoBehaviour {

    public List<string> tagsToCollideWith;

    [SerializeField]
    private float sizeDeltaPerSecond;
    private bool currentlyGrowing;

    private IEnumerator growthCoroutine;

    private Transform tr;	
    
    void Start () {
        growthCoroutine = grow();
        currentlyGrowing = false;

        tr = GetComponent<Transform>();
	}

    void Update()
    {
        if (tr.localScale.x <= 0.1f &&
            tr.localScale.y <= 0.1f)
        {
            destroy();
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        { 
            currentlyGrowing = true;
            StartCoroutine(growthCoroutine);            
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            currentlyGrowing = false;
            StopCoroutine(growthCoroutine);
        }
    }

    private IEnumerator grow()
    {
        while (currentlyGrowing)
        {
            tr.DistortScaleXByDelta(sizeDeltaPerSecond / 10);
            tr.DistortScaleYByDelta(sizeDeltaPerSecond / 10);
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void destroy()
    {
        Death.die(this.gameObject);
    }
}
