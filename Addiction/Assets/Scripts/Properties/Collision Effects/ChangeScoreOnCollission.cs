﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ScoreChangeType
{
    INCREASE,
    DECREASE
}

public class ChangeScoreOnCollission : MonoBehaviour {

    public List<string> tagsToCollideWith;

    public float scoreChangeAmount;
    public bool repeatActionOverTime;
    public float repeatingIntervalSeconds;
    public ScoreChangeType scoreChangeType;

    private IEnumerator scoreCoroutine;
    private float scoreChangeSign;
    private Transform playerCollidedWith;

    void Start()
    {
        setupScoreCoroutine();
    }

    private void setupScoreCoroutine()
    {
        scoreCoroutine = changeScoreRepeating();
        switch (scoreChangeType)
        {
            case ScoreChangeType.INCREASE:
                scoreChangeSign = 1;
                break;
            case ScoreChangeType.DECREASE:
                scoreChangeSign = -1;
                break;
        }
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            playerCollidedWith = col.transform;
            StartCoroutine(scoreCoroutine);
        }
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        if (tagsToCollideWith.Contains(col.tag))
        {
            playerCollidedWith = col.transform;
            StopCoroutine(scoreCoroutine);
        }
    }

    private IEnumerator changeScoreRepeating()
    {
        changeScore();
        while (repeatActionOverTime)
        {
            yield return new WaitForSeconds(repeatingIntervalSeconds);
            changeScore();
        }

    }
    private void changeScore()
    {
        Messenger<float>.Broadcast(
                playerCollidedWith.name + " changeScore", scoreChangeAmount * scoreChangeSign);
    }
}
