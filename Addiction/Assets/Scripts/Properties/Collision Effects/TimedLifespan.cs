﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TimedLifespan : MonoBehaviour {

    public float initialLifespan;
    public List<string> tagsThatResetLifespan;
    [SerializeField]
    private float currentLifespan;

	void Start ()
    {
        resetLifespan();
	}

    void FixedUpdate()
    {
        currentLifespan -= Time.deltaTime;
        if (currentLifespan < 0.0)
            Death.die(this.gameObject);
    }

    public void OnTriggerStay2D(Collider2D col)
    {
        if (tagsThatResetLifespan.Contains(col.tag))
        {
            resetLifespan();
        }
    }

    private void resetLifespan()
    {
        currentLifespan = initialLifespan;
    }

    
}
