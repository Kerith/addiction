﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using KWorks.Wrappers;

public enum Relation
{
    DIRECTLY_PROPORTIONAL,
    INVERSELY_PROPORTIONAL
}

[System.Serializable]
public class SpawnableEntity
{
    public GameObject prefab; 
    public float healthModifier;
    public float cumulativeFrequency;
    public Relation relationshipToHealth;
    public float minSpawnSizeNorm = 1;
    public float maxSpawnSizeNorm = 1;

    public float getHealthModifier()
    {
        float proportionality = 1;
        switch(relationshipToHealth)
        {
            case Relation.DIRECTLY_PROPORTIONAL:
                proportionality = 1;
                break;
            case Relation.INVERSELY_PROPORTIONAL:
                proportionality = -1;
                break;
        }

        return Mathf.Pow(healthModifier, proportionality);
    }
}

public class BoundingSpawner : MonoBehaviour {

    public float timeUntilFirstSpawn;
    public int maxEntityCount;
    public Vector2 spawnBounds;
    public float entitySpawnRate;
    public List<SpawnableEntity> entitiesToSpawn;    

    [SerializeField]
    private List<GameObject> entitiesAccountedFor;
    [SerializeField]
    private float maxFrequency;

    private Transform tr;
    private CircleCollider2D coll;
    private Health health;

    void Start()
    {
        initializeComponents();
        Invoke("startSpawning", timeUntilFirstSpawn);
    }

    private void startSpawning()
    {
        InvokeRepeating("spawnEntities", 0, entitySpawnRate);
    }

    private void initializeComponents()
    {
        tr = GetComponent<Transform>();
        coll = GetComponent<CircleCollider2D>();
        health = GetComponentInParent<Health>();
    }   

    private void spawnEntities()
    {
        if (entitiesAccountedFor.Count >= maxEntityCount)
            return;
        
        int newEntities = Random.Range(0, maxEntityCount - entitiesAccountedFor.Count);
        for (int i = 0; i < newEntities; i++)
        {
            float targetFrequency = Random.Range(0, maxFrequency);

            SpawnableEntity entity = entitiesToSpawn.Find(x => x.cumulativeFrequency >= targetFrequency);
            GameObject prefab = entity.prefab;
            Vector3 newPosition = calculateNewEntityPosition();           

            GameObject instance = (GameObject)Instantiate(prefab, newPosition, Quaternion.identity);
            float newScale = Random.Range(entity.minSpawnSizeNorm, entity.maxSpawnSizeNorm);
            instance.GetComponent<Transform>().SetScaleXProportional(newScale);
            instance.GetComponent<Transform>().SetScaleYProportional(newScale);
        }
    }

    private Vector3 calculateNewEntityPosition()
    {
        float distance = Random.Range(spawnBounds.x, spawnBounds.y);
        Vector3 direction = Vector3.Normalize(new Vector3(
               Random.Range(-1f, 1f),
               Random.Range(-1f, 1f),
               0.0f));
                
        return direction * distance + tr.position;
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        accountFor(col.gameObject);
        listenForDeathOf(col.gameObject);
    }

    public void OnTriggerExit2D(Collider2D col)
    {
        unaccountFor(col.gameObject);
        stopListeningForDeathOf(col.gameObject);
    }

    private void listenForDeathOf(GameObject entity)
    {
        Messenger<GameObject>.AddListener(entity.name + " died", unaccountFor);
    }

    private void stopListeningForDeathOf(GameObject entity)
    {
        Messenger<GameObject>.RemoveListener(entity.name + " died", unaccountFor);
    }

    private void accountFor(GameObject entity)
    {
        entitiesAccountedFor.Add(entity);
        updateSpawnableListFrequencies();
    }  

    private void unaccountFor(GameObject entity)
    {
        entitiesAccountedFor.Remove(entity);
        updateSpawnableListFrequencies();
    }

    private void updateSpawnableListFrequencies()
    {
        int entityCount = entitiesAccountedFor.Count;
        float currentHealth = health.getHealth();

        float totalFrequency = 0.0f;

        foreach (SpawnableEntity se in entitiesToSpawn)
        {
            float ocurrences = entitiesAccountedFor.FindAll(x => x.name == se.prefab.name).Count;
            float rawFrequency = (1 - ocurrences / entityCount);            
            float healthModifier = se.healthModifier * se.getHealthModifier();
            totalFrequency += (rawFrequency * healthModifier);

            se.cumulativeFrequency = totalFrequency;
        }

        maxFrequency = totalFrequency;
    }
}
