﻿using UnityEngine;
using System.Collections;
using KWorks.Wrappers;

// Idea From: http://answers.unity3d.com/answers/372941/view.html
// But heavily changed

public class TextureScroll : MonoBehaviour {

    public Transform myCamera;
    public float backgroundSpeed = 1.0f;

    private Transform tr;
    private Renderer rend;

    void Start()
    {
        tr = GetComponent<Transform>();
        rend = GetComponent<Renderer>();
    }

    void Update()
    {
        Vector3 dir = tr.position - myCamera.position;
        Vector2 aux = rend.material.mainTextureOffset;
        aux += new Vector2
            (backgroundSpeed * dir.x * Time.deltaTime, 
            backgroundSpeed * dir.y * Time.deltaTime);
        rend.material.mainTextureOffset = aux;
    }
}
