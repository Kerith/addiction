﻿using UnityEngine;
using System.Collections;

public class InputUtils : MonoBehaviour {

	public static Vector3 mousePositionInWorld()
	{
		Vector3 inputMousePosition = Input.mousePosition;
		inputMousePosition.z = 10.0f;
		return Camera.main.ScreenToWorldPoint(inputMousePosition);
	}

	public static Vector3 pointingDirectionOfJoysticks()
	{
		return new Vector3 (Input.GetAxis("Joystick Horizontal"), 
							(-1) * Input.GetAxis("Joystick Vertical"), 
							0);
	}

	public static Vector3 directionTowardsMouse(Vector3 startingPosition) 
	{
		return Vector3.Normalize(InputUtils.mousePositionInWorld () - startingPosition);
	}	
}
