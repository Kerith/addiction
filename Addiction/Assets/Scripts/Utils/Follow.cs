﻿using UnityEngine;
using System.Collections;
using KWorks.Wrappers;

public class Follow : MonoBehaviour {

    public string targetTag;

    private Transform target;
    private Transform tr;

	void Start () {
        tr = GetComponent<Transform>();
        target = GameObject.FindGameObjectWithTag(targetTag).
                    GetComponent<Transform>();
	}
	
	void Update () {
        tr.SetX(target.position.x);
        tr.SetY(target.position.y);
	}
}
