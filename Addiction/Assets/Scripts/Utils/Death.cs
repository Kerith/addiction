﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour {
   
    public static void die(GameObject go)
    {
        Destroy(go);
        Messenger<GameObject>.Broadcast(go.name+" died", go);       
    }

}
