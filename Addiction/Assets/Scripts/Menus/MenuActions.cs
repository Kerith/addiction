﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

// Techniques from the 4th practice

public class MenuActions : MonoBehaviour {


    private AudioSource aud;

	void Start () {

        aud = GetComponent<AudioSource>();
	}
	
    public void StartGame()
    {
        aud.Play(0);
        Invoke("loadLevel", 3.5f);
    }

    private void loadLevel()
    {
        SceneManager.LoadScene("Level");
    }
}
