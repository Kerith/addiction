﻿using UnityEngine;
using System.Collections;

public class PointScore : MonoBehaviour {

    [SerializeField]
    private float currentScore;

    void Start()
    {
        currentScore = 0;

        initializeListeners();
    }

    private void initializeListeners()
    {
        Messenger<float>.AddListener(this.gameObject.name + " changeScore", updateScore);
        Messenger.AddListener<float>(this.gameObject.name + " requestScore", getScore);
    }

    private float getScore()
    {
        return currentScore;
    }

    private void updateScore(float scoreDelta)
    {
        currentScore += scoreDelta;
        Messenger<float>.Broadcast(this.gameObject.name + " scoreUpdate", currentScore);
    }
}
