﻿using UnityEngine;
using System.Collections;
using System;

public class SurvivalScore : MonoBehaviour {

    [SerializeField]
    private float scoreIncreasePerSecond;
    [SerializeField]
    private float currentScore;

	void Start ()
    {
        currentScore = 0;

        initializeListeners();
        InvokeRepeating("increaseScore", 0, 0.1f);
	}

    private void initializeListeners()
    {
        Messenger.AddListener<float>(this.gameObject.name + " requestScore", getScore);
    }

    private float getScore()
    {
        return currentScore;
    }

    private void increaseScore()
    {
        currentScore += scoreIncreasePerSecond / 10.0f;
        Messenger<float>.Broadcast(this.gameObject.name + " scoreUpdate", currentScore);
    }
}
