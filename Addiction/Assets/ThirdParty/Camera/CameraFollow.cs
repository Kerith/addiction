using UnityEngine;

[RequireComponent (typeof (Camera))]
public class CameraFollow : MonoBehaviour {
	public Transform target;

	private Transform camtrf;
	private Camera cam;

	[SerializeField]
	private float yOffset = 0f;

	private void Start() {
		camtrf = GetComponent<Transform>();
		cam = GetComponent<Camera>();
	}

	private void Update() {
		camtrf.position = new Vector3(target.position.x, target.position.y + (yOffset * cam.orthographicSize), target.position.z - 10);
	}
}