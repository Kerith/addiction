using System;
using UnityEngine;

[RequireComponent (typeof (Camera))]
public class ZoomPlayer : MonoBehaviour {
    public float zoomSpeed = 1;
    public float targetOrtho;
    public float smoothSpeed = 2.0f;
    public float minOrtho = 1.0f;
    public float maxOrtho = 20.0f;

    public float maxAttributeForZoom;

    [SerializeField]
    private Transform target;

    private Camera cam;
	private Transform position;

    private float currentTargetAttribute;
    private float orthoDelta;

    private void Start() {
		cam = GetComponent<Camera>();
		position = GetComponent<Transform>();

        targetOrtho = cam.orthographicSize;

        Messenger.Broadcast<float>(target.name+" requestHealth", initializeTargetAttribute);
        Messenger<float>.AddListener(target.name+" healthChanged", updateTargetAttribute);		
	}

    void Update()
    {
        updateCameraZoom();        
    }

    private void initializeTargetAttribute(float initialValue)
    {        
        currentTargetAttribute = initialValue;
        updateCameraZoom();
    }  

    private void updateTargetAttribute(float attributeDelta)
    {        
        currentTargetAttribute += attributeDelta;
        currentTargetAttribute = Mathf.Clamp(currentTargetAttribute, 0, maxAttributeForZoom);
        orthoDelta = -Mathf.Sign(attributeDelta) * currentTargetAttribute / maxAttributeForZoom;
    }

    private void updateCameraZoom()
    {        
        if (orthoDelta != 0.0f)
        {
            targetOrtho -= orthoDelta * zoomSpeed;
            targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho);
            orthoDelta = 0;
        }

        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
    }
}