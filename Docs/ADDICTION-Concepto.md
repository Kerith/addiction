# ADDICTION

- **_Práctica:_** Proyecto Final (DVI)
- **_Alumno:_** Borja Lorente Escobar
- **_DNI:_** 73140789F

## Ficha del Juego

- **_Título:_** _Addiction_
- **_Género:_** Art Game
- **_Motor:_** Unity3D
- **_Plataformas:_** Windows, Linux, WebGL
- **_Modos:_** Un jugador

## Descripción

_Addiction_ es una metáfora sobre el proceso que sigue alguien que ha pasado por una adicción, o incluso simplemente un mal hábito. Mediante sus mecáncias, _Addiction_ te permite **_experimentar de primera mano_** las sensaciones a las que se somete un adicto, y te guía en cualquiera que sea el camino que quieras recorrer.

En _Addiction_ puedes sentir tanto el subidón inicial de la primera dosis como la caída en la espiral autodestructiva del pacto fáustico que son estos malos hábitos, e incluso el proceso de recuperación, si es que alguna vez hubo alguno.

Este juego se puede tomar de muchas formas. Habrá gente que se sienta identificada, y gente que se sienta indiferente al jugarlo. Pero el objetivo es el de servir de advertencia a aquellos tentados en hacer tratos con el diablo.

## Mecánicas Centrales

### El Juego

El juego se desarrolla en un solo nivel infinito, con el fondo plano. En ese nivel se van generando aleatoriamente entidades, de los tipos descritos a continuación. El jugador navega ese nivel usando las teclas de dirección.

Al principio todo el tablero está lleno de _salvaciones_ y _dosis_, pero esto puede cambiar

### El jugador

El jugador es una sola entidad que puede moverse en cualquier dirección, usando impulsos de fuerza, dirigidos por el ratón, que se dispara cada vez que el usuario pulsa el botón de avanzar.

Se cuenta con un atributo de _salud_, el cual indica la medida en la que la adicción afecta al jugador. La _salud_ se representa mediante el tamaño del personaje, así como el brillo de su color y la fuerza que le es posible ejercer en cada uno de sus impulsos.

Cuanta menos _salud_ tenga el personaje, más fuerza ejerce en sus impulsos, pero más se ve atraído hacia las _tentaciones_ (más adelante). Además, el brillo de su sprite es directamente proporcional a la _salud_ del personaje.

La _salud_ del personaje también influye en la frecuencia de aparición de las distintas entidades en el mapa, como se describe a continuación.

### Otras entidades

#### Tentaciones

Son las principales representaciones de la adicción en el juego. Se representan como agujeros negros que atraen al jugador, y que decrementan su _salud_ cada instante que permanece en ellos, a la vez que ellos mismos crecen al succionar la _salud_ del jugador, y se mueven hacia él.

Su tamaño, frecuencia de aparición, velocidad de movimiento y fuerza de atracción que ejercen sobre el jugador son inversamente proporcionales a su _salud_.

#### Salvaciones

Simbolizan los amigos, la familia, y las buenas costumbres que ayudan al jugador a salir de su adicción. Se representan con bolas de luz clara, que sanan al jugador cada vez que éste entra en ellas. Sin embargo, la carga de ayudar a alguien a superar la adicción pesa sobre cualquiera, y por lo tanto, estas entidades se encogerán a medida que se sana el jugador, hasta desaparecer.

El tamaño, poder curativo y frecuencia de aparición de estas entidades es directamente proporcional a la _salud_ del personaje.

Si una _tentación_ llega a tocar una _salvación_, la _salvación_ será inmediatamente destruida.

#### Dosis

Simbolizan el acuerdo fáustico que hacen todos los adictos, mediante el cual obtienen un beneficio a corto plazo (una incremento temporal de la fuerza de impulso), a cambio de un detrimento a largo plazo (una reducción en la _salud_).

Las _dosis_ se presentan en forma de _pickups_ en todo el mapa, y su tamaño, atributos y frecuencia de aparición son constantes a lo largo del juego.

### Condiciones de Victoria

Realmente no debería haber condiciones de victoria, pero dado que es un requisito de la práctica, se puede considerar como victoria que el jugador llegue a tener fuerza de impulso 0.

## Referentes

_Addiction_ es una expansión y refinamiento de la prueba de concepto lanzada en OUYA (<https://goo.gl/gmM1Li>).
