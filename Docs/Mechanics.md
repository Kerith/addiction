# Addiction


- The Board

  - The player navigates a board with good and bad guys.
  - During movement, and with time, more Guys appear.
  - The density and ratio of Good/Bad guys depends on the state of the player.

- The player

  - Moves in all directions.
  - Has a "Health" variable, represented by it's size, color...
  - Gets "Infected"/"Healed" when he stands inside Good/Bad guys.

    - Infected: Slower more clunky movement, weaker force push, smaller.
    - Healed: Faster more agile movement, brighter color, bigger.

![](./Field.jpg)

- The "Good guys"

  - They heal the player
  - When the player is in them, they shrink until they disappear.
  - Some of them randomly turn into "Bad guys". This probability is inversely proportional to the player's health.

- The "Bad guys"

  - They Infect the player.
  - When the player is in them they grow.
  - When they touch a Good guy, they swallow it.

![](./Mechanics.jpg)

- The "Pills" (Faustian Bargain)

  - They are collectibles, that provide a boost in speed, at the expense of Infecting the player.

- Victory Condition

  - None, for the moment.
